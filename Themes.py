#-*- coding: utf-8 -*-
""" 
Themes macro - an easy way to try themes
========================================

This macro with together with theme action, let you try themes without
going to UserPreferences, selecting, saving, refreshing, selecting
another... and so on.
    

Install
-------

Put in your wiki/data/plugin/macro/

    
Compatibility
--------------
Tested with release 1.3.5, should work with any 1.3 release.


Legal
-----
@copyright � 2005 Nir Soffer <nirs@freeshell.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# This macro depends on the installed themes
Dependencies = ['time']


class ThemesMacro:
    """ Themes - list all installed themes 
    
    Usage:
    
        [[Themes]]
        
    Create a list of links to all installed themes. Click each link to
    view current page with the selected theme.

    To set a preferred theme, visit UserPrefereces page and select the
    theme from the list of themes.
    """
    
    def __init__(self, macro, args):
        self.macro = macro
        self.args = args
        self.request = macro.request
        
    def execute(self):
        if self.args:
            return self.formatRawMarkup() 
        from MoinMoin import wikiutil
        themes = wikiutil.getPlugins('theme', self.request.cfg)
        return self.format(themes)

    def formatRawMarkup(self):
        """ Return the raw markup that invoked this macro
        
        This is little stupid, the parser has this text and can just
        render it as text, but it does not.
        """
        return '[[Themes(%s)]]' % self.macro.formatter.text(self.args)

    def format(self, themes):
        """ Format list of theme links
        
        TODO: create a gallery with thumbnails of themes. Each theme
        can include a small screenshot, and it can be formatted in a
        table.
        """
        f = self.macro.formatter
        items = []
        items = [f.bullet_list(1),
                 self.formatThemes(themes),
                 f.bullet_list(0),]
        return ''.join(items)

    def formatThemes(self, themes):
        """ Format themes links
        
        No need to use pagelink to the current page
        """
        f = self.macro.formatter
        pageURL = self.request.page.url(self.request, escape=0)
        links = []
        themes.sort()
        for themeName in themes:
            url = '%s?action=theme&theme=%s' % (pageURL, themeName)
            link = [f.listitem(1),
                    f.url(1, url),
                    f.text(themeName),
                    f.url(0),
                    f.listitem(0)]
            links.append(''.join(link))
        return ''.join(links)


def execute(macro, args):
    return ThemesMacro(macro, args).execute()
