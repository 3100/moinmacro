import logging
from MoinMoin import wikiutil
from MoinMoin.Page import Page
Dependencies=[]

def execute(macro, args):
  request = macro.request
  pagename = macro.formatter.page.page_name
  url = "http://wiki.3100.co" + wikiutil.escape(Page(request, pagename).url(request))
  logging.info("pagename:%s\n" % pagename)
  logging.info("url:%s\n" % url)
  logging.info("")
  return macro.formatter.rawHTML('<div id="fb-root"></div><script src="http://connect.facebook.net/ja_JP/all.js#xfbml=1"></script><fb:comments href="%s" num_posts="2" width="500"></fb:comments>' % url)
