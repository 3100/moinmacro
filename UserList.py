# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - User account Listing

    (mostly copied from admin.do_user_browser)
    @copyright: 2001-2004 Juergen Hermann <jh@web.de>,
                2003-2007 MoinMoin:ThomasWaldmann,
                2007 MoinMoin:ReimarBauer,
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin import user, wikiutil
from MoinMoin.util.dataset import TupleDataset, Column
from MoinMoin.Page import Page


def macro_UserList(macro):
    """ macro to list users. """
    request = macro.request
    _ = request.getText

    data = TupleDataset()
    data.columns = [
        Column('name', label=_('Username')),
        Column('email', label=_('Email')),
        Column('groups', label=_('Member of Groups')),
        Column('language', label=_('Language')),
    ]

    isgroup = request.cfg.cache.page_group_regexact.search
    groupnames = request.rootpage.getPageList(user='', filter=isgroup)

    # Iterate over users
    for uid in user.getUserList(request):
        account = user.User(request, uid)
        # don't offer groupnames to users which aren't allowed to read them
        grouppage_links = ', '.join([Page(request, groupname).link_to(request)
                                     for groupname in groupnames
                                     if request.dicts.has_member(groupname, account.name) and request.user.may.read(groupname)])

        userhomepage = Page(request, account.name)
        if userhomepage.exists():
            namelink = userhomepage.link_to(request)
        else:
            namelink = wikiutil.escape(account.name)

        if account.disabled:
            enable_disable_link = request.page.link_to(
                                    request, text=_('Enable user'),
                                    querystr={"action": "userprofile",
                                              "name": account.name,
                                              "key": "disabled",
                                              "val": "0",
                                             },
                                    rel='nofollow')
            namelink += " (%s)" % _("disabled")
        else:
            enable_disable_link = request.page.link_to(
                                    request, text=_('Disable user'),
                                    querystr={"action": "userprofile",
                                              "name": account.name,
                                              "key": "disabled",
                                              "val": "1",
                                             },
                                    rel='nofollow')

        if account.email:
            email_link = (request.formatter.url(1, 'mailto:' + account.email, css='mailto') +
                          request.formatter.text(account.email) +
                          request.formatter.url(0))
        else:
            email_link = ''

        # language defined in settings or default language
        language = account.language or request.cfg.language_default

        data.addRow((
            request.formatter.rawHTML(namelink),
            email_link,
            request.formatter.rawHTML(grouppage_links),
            language,
        ))

    if data:
        from MoinMoin.widget.browser import DataBrowserWidget

        browser = DataBrowserWidget(request)
        browser.setData(data)
        return browser.toHTML()

    # No data
    return ''

