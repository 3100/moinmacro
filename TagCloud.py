# -*- coding: iso-8859-1 -*-
"""
   MoinMoin - TagCloud

   Create tagcloud, example: http://www.iam-wiki.org

   @copyright: 2007 by Christian Groh
"""

import re
from MoinMoin import search
from MoinMoin.Page import Page
from MoinMoin.PageEditor import PageEditor

Dependencies = ["namespace"]

def execute(macro, args):

   request = macro.request

   # get params
   if args:
      args = args.split(',')
   else:
      args = []

   kw = {}
   for arg in args :
      if '=' in arg:
         key, value = arg.split('=', 1)
         kw[str(key.strip())] = value.strip()

   try:
      maxTags = int( kw["maxTags"] )
   except (KeyError, ValueError):
      maxTags = 50

   try:
      autoCreate = kw["autoCreate"]
      if autoCreate == "true" or autoCreate == "True":
         autoCreate = True
      else:
         autoCreate = False

   except (KeyError):
      autoCreate = False

   #{level:hits , level:hits , ...}
   level = { 0 : 4 , 1 : 7 , 2 : 12 , 3 : 18 , 4 : 25 , 5 : 35 , 6 : 50 , 7 : 60 , 8 : 90 }

   args = r'regex:((\r)?\n----(\r)?\n[a-zA-Z])'

   # Search the pages
   query = search.QueryParser().parse_query(args)
   results = search.searchPages(request, query)
   pages = [hit.page_name for hit in results.hits]

   tags = []

   for page in pages:
      page = Page(request, page)
      if page.isStandardPage() and not page.isUnderlayPage():
         body = page.get_raw_body()
         match = re.search(r'----(\r)?\n(?P<tags>.*)(\r)?\n', body)
         match = match.group('tags')
         match = match.split(',')
         for tag in match:
            tags.insert(0, (str(tag)).strip())

   taglist = []
   taglist = list(frozenset(tags))

   def sort(t):
      return t[1]

   show = []
   for tag in taglist:
      show.append( (tag, tags.count(tag)) )
   show.sort(key=sort, reverse=True)
   show = show[0:maxTags]
   show.sort()

   html = []

   for tag in show:

      pagename = tag[0]
      hits = tag[1]

      # auto create tag page if not exist
      if autoCreate:
         page = Page(request, pagename)
         if page.isStandardPage(includeDeleted=False) == False and page.isUnderlayPage() == False:

            from MoinMoin.security import Permissions
            class SecurityPolicy(Permissions):
               def write(*args, **kw):
                  return True
               def save(*args, **kw):
                  return True
            request.user.may = SecurityPolicy(request.user)

            PageEditor(request, pagename).saveText(ur"[[FullSearch(regex:(-{4}(\r)?\n(.*)%s))]]"%(tag[0]), 0)

      #level0
      if hits < level[0]:
         html.append(u'<span  style="font-size:0.65em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level1
      elif hits < level[1]:
         html.append(u'<span  style="font-size:0.75em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level2
      elif hits < level[2]:
         html.append(u'<span  style="font-size:0.9em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level3
      elif hits < level[3]:
         html.append(u'<span  style="font-size:1.0em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level4
      elif hits < level[4]:
         html.append(u'<span  style="font-size:1.05em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level5
      elif hits < level[5]:
         html.append(u'<span  style="font-size:1.1em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level6
      elif hits < level[6]:
         html.append(u'<span  style="font-size:1.15em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level7
      elif hits < level[7]:
         html.append(u'<span  style="font-size:1.2em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level8
      elif hits < level[8]:
         html.append(u'<span  style="font-size:1.25em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))
      #level9
      else:
         html.append(u'<span  style="font-size:1.3em;"><a href="%s"> %s</a></span>'%(pagename, tag[0]))

   return ''.join(html)